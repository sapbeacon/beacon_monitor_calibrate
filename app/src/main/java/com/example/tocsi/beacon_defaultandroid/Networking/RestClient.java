package com.example.tocsi.beacon_defaultandroid.Networking;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.Toast;

import com.example.tocsi.beacon_defaultandroid.EventBus.EventBus;
import com.example.tocsi.beacon_defaultandroid.EventBus.NetworkEvent;
import com.example.tocsi.beacon_defaultandroid.Manager.ContentManager;
import com.example.tocsi.beacon_defaultandroid.Model.BeaconResponse;
import com.example.tocsi.beacon_defaultandroid.Model.Point;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Tocsi on 2015.11.19..
 */
public class RestClient {

    private RequestInterceptor interceptor;
    public static final String BASE_URL = "https://laptopc5234335trial.hanatrial.ondemand.com/Tocsi_PC_Hana_Servlet";

    private static RestClient ourInstance = new RestClient();

    public static RestClient getInstance() {
        return ourInstance;
    }

    private RestClient() {
    }


    public void getBeaconData(String mapid){
        RestService.BeaconService beaconService = createAdapter().create(RestService.BeaconService.class);
        beaconService.getBeacons(mapid,new Callback<BeaconResponse>() {
            @Override
            public void success(BeaconResponse beaconResponse, Response response) {
                Log.e("Sucess","yeah");
                EventBus.getInstance().post(new NetworkEvent("RestRequest Sucess!"));
                ContentManager.getInstance().setmReferencePoints(beaconResponse.getPoints());
                new ImageDownloadAsynctask().execute(beaconResponse.getMapImageUrl());


            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("getbeacon","resst_failed");
                EventBus.getInstance().post(new NetworkEvent("RestRequest Failed! GO debug..."));

            }
        });
    }

    public void postPointData(Point json, String mapid){
        RestService.BeaconService beaconService = createAdapter().create(RestService.BeaconService.class);
        beaconService.postPointData(mapid, json, new Callback<BeaconResponse>() {
            @Override
            public void success(BeaconResponse beaconResponse, Response response) {
                EventBus.getInstance().post(new NetworkEvent("RestRequest POST Sucess!"));
            }

            @Override
            public void failure(RetrofitError error) {
                EventBus.getInstance().post(new NetworkEvent("RestRequest POST Failed! GO debug..."));
            }
        });
    }

    public void deleteAllData(){
        RestService.BeaconService beaconService = createAdapter().create(RestService.BeaconService.class);
        beaconService.deleteData(new Callback<BeaconResponse>() {
            @Override
            public void success(BeaconResponse beaconResponse, Response response) {
                EventBus.getInstance().post(new NetworkEvent("RestRequest DELETE Sucess!"));
            }

            @Override
            public void failure(RetrofitError error) {
                EventBus.getInstance().post(new NetworkEvent("RestRequest DELETE Failed! GO debug..."));
            }
        });
    }



    private RestAdapter createAdapter() {
        return new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                //.setRequestInterceptor(interceptor)
                .setEndpoint(BASE_URL).build();
    }

    private RequestInterceptor createRequestInterceptor() {
        return new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
            }
        };
    }

    public Bitmap getBitmapFromURL(String src) {

        try {
            java.net.URL url = new java.net.URL(src);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
