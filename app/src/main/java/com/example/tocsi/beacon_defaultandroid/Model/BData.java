package com.example.tocsi.beacon_defaultandroid.Model;

import java.util.List;

/**
 * Created by Tocsi on 2015.11.23..
 */
public class BData {
    String beaconname;
    int[] meresdata;

    public String getBeaconname() {
        return beaconname;
    }

    public void setBeaconname(String beaconname) {
        this.beaconname = beaconname;
    }

    public int[] getMeresdata() {
        
        return meresdata;
    }

    public void setMeresdata(int[] meresdata) {
        this.meresdata = meresdata;
    }
}
