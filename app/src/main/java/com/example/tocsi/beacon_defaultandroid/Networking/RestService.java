package com.example.tocsi.beacon_defaultandroid.Networking;

import com.example.tocsi.beacon_defaultandroid.Model.BeaconResponse;
import com.example.tocsi.beacon_defaultandroid.Model.Point;

import org.json.JSONObject;

import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.Callback;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by Tocsi on 2015.11.19..
 */
public class RestService {

    public interface BeaconService{

        @GET("/TocsiLaptop")
        void getBeacons(@Query("mapid") String mapid, Callback<BeaconResponse> beaconResponseCallback);

        @POST("/TocsiLaptop")
        void postPointData(@Header("mapid") String mapid,@Body Point point, Callback<BeaconResponse> beaconResponseCallback);

        @DELETE("/TocsiLaptop")
        void deleteData(Callback<BeaconResponse> beaconResponseCallback);
    }
}
