package com.example.tocsi.beacon_defaultandroid.Service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;

import com.example.tocsi.beacon_defaultandroid.Manager.BleScanManager;

public class ScannService extends Service {
    public ScannService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        final Handler handler = new Handler();
        Runnable scannBeacon = new Runnable() {
            @Override
            public void run() {
                BleScanManager.getInstance().startFullBleScan(getApplicationContext(), true);
                handler.postDelayed(this,5000);
            }
        };
        handler.post(scannBeacon);
        return super.onStartCommand(intent, flags, startId);
    }
}
