package com.example.tocsi.beacon_defaultandroid.Manager;

/**
 * Created by Tocsi on 2015.10.29..
 */
public class BeaconMaths {
    private static BeaconMaths ourInstance = new BeaconMaths();

    public static BeaconMaths getInstance() {
        return ourInstance;
    }

    private BeaconMaths() {
    }

    static final char[] hexArray = "0123456789ABCDEF".toCharArray();



//region maths

    public double getDistance_Average_Ratio(double average, int txPower){

        double ratio = average*1.0/txPower;
        if (ratio < 1.0) {
            return Math.pow(ratio,10);
        }
        else {
            double accuracy =  ((0.89976)*Math.pow(ratio,7.7095) + 0.111);
            return accuracy;
        }

    }



    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

//endregion
}
