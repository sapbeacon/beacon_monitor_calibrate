package com.example.tocsi.beacon_defaultandroid.UI;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.tocsi.beacon_defaultandroid.EventBus.EventBus;
import com.example.tocsi.beacon_defaultandroid.EventBus.TargetBeaconMeasuredEvent;
import com.example.tocsi.beacon_defaultandroid.Manager.BleScanManager;
import com.example.tocsi.beacon_defaultandroid.R;
import com.squareup.otto.Subscribe;

public class CalibrateActivity extends AppCompatActivity {

    private Button btnReady;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calibrate);
        btnReady = (Button) findViewById(R.id.button5);
        String min = getIntent().getStringExtra("minorValue");
        final int minortoCalib = Integer.parseInt(min);

        btnReady.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //progress = ProgressDialog.show(getApplicationContext(), "Calibrating","Calibrating your beacon, please wait...", true);
                progressDialog = ProgressDialog.show(CalibrateActivity.this, "Calibrating beacon", "Please wait...");

                BleScanManager.getInstance().startTargetedBleScan(getApplicationContext(),true,minortoCalib);
                Handler h = new Handler();


            }
        });

    }

    @Subscribe
    public void onCalibrationFinished(TargetBeaconMeasuredEvent event){
        Toast.makeText(getApplicationContext(),"Calibration finished!",Toast.LENGTH_SHORT).show();
        progressDialog.dismiss();
        finish();
    }

    @Override
    protected void onStart() {
        EventBus.getInstance().register(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        EventBus.getInstance().unregister(this);
        super.onStop();
    }
}
