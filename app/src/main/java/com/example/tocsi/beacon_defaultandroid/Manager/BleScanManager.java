package com.example.tocsi.beacon_defaultandroid.Manager;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.example.tocsi.beacon_defaultandroid.EventBus.AllBeaconsFoundEvent;
import com.example.tocsi.beacon_defaultandroid.EventBus.EventBus;
import com.example.tocsi.beacon_defaultandroid.EventBus.TargetBeaconMeasuredEvent;
import com.example.tocsi.beacon_defaultandroid.Model.BLEDevice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Tocsi on 2015.10.29..
 */
public class BleScanManager {

    private BluetoothAdapter mBluetoothAdapter;
    private long SCAN_PERIOD = 10000;
    private List<BLEDevice> deviceList;
    private Map<String,BLEDevice> bleDeviceMap;
    private List<ScanFilter> filters;
    private ScanSettings settings;
    private Set<String> deviceSet;
    private int targetMinor = 0;
    //LEscanner for api level 21+
    private BluetoothLeScanner mLEScanner;
    private ScanCallback mHighAiLevelCallback_FullScan;
    private ScanCallback mHighAiLevelCallback_TargetedScan;

    
    
    private static BleScanManager ourInstance = new BleScanManager();

    public static BleScanManager getInstance() {
        return ourInstance;
    }

    private BleScanManager() {
        deviceList = new ArrayList<BLEDevice>();
        bleDeviceMap = new HashMap<String,BLEDevice>();
        deviceSet = new HashSet<String>();
        filters = new ArrayList<ScanFilter>();



    }


    
//region public Methods

    public void setScanPeriod(long sec){
                SCAN_PERIOD = sec*1000;
    }

    //region StartFullScan
    public void startFullBleScan(final Context context, final boolean enable) {
        deviceList = new ArrayList<BLEDevice>();
        bleDeviceMap = new HashMap<String,BLEDevice>();
        deviceSet = new HashSet<String>();

        //initialize components
        final BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        Handler mHandler = new Handler();
        if (Build.VERSION.SDK_INT >= 21) {
            settings = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                    .build();
            mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
            mHighAiLevelCallback_FullScan = scanCallbackHighAPILevelFullScan();
        }

        //-----------------------


        if (enable) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Scan Completed after given period
                    if (Build.VERSION.SDK_INT < 21) {
                        mBluetoothAdapter.stopLeScan(scanCallBackLowAPILevelFullScan);
                    } else {
                        mLEScanner.stopScan(mHighAiLevelCallback_FullScan);
                    }
                    //Make arraylist with BLEDevices for listView Adapter.
                    ArrayList<BLEDevice> devicesFoundDuringScann = new ArrayList<BLEDevice>();
                    for(BLEDevice device: bleDeviceMap.values()){
                        devicesFoundDuringScann.add(device);
                    }
                    //---------------------------------------------
                    //Cut each device's rssiValues-----------------
                    //Bottom 30% left out and the top 1 value
                    for (BLEDevice d : devicesFoundDuringScann){

                        //Check Content manager for txPower. If exists than we use it instead.
                        int actualDeviceMinor = d.getMinor();
                        int managersDataOfTxPower = ContentManager.getInstance().getBeaconTxPower(context,Integer.toString(actualDeviceMinor));
                        if( managersDataOfTxPower != 0){
                            d.setTxPower(managersDataOfTxPower);
                        }
                        //-------
                        //Collections.sort(d.getRssiValues());
                        //List<Integer> temp = d.getRssiValues();
                        //int k = d.getRssiValues().size();
                        //int min = Math.round(k * 0.3f);
                        //List<Integer> temp2 = temp.subList(min,k-1);
                        //d.setRssiValues(temp2);
                    }
                    //----------------------------------------------
                    //TODO check all devices and set TXPOWER if it was saved before in Content Manager
                    EventBus.getInstance().post(new AllBeaconsFoundEvent(devicesFoundDuringScann));

                }
            }, SCAN_PERIOD);


            //Start scanning--------------------------------------------------
            if (Build.VERSION.SDK_INT < 21) {
                mBluetoothAdapter.startLeScan(scanCallBackLowAPILevelFullScan);
            } else {
                mLEScanner.startScan(filters, settings, mHighAiLevelCallback_FullScan);
            }
            //----------------------------------------------------------------

        }
        // If not enabled--
        else {
            if (Build.VERSION.SDK_INT < 21) {
                mBluetoothAdapter.stopLeScan(scanCallBackLowAPILevelFullScan);
            } else {
                mLEScanner.stopScan(mHighAiLevelCallback_FullScan);

            }
        }
        //-----------------
    }
    //endregion

    public void clearDeviceList(){

        deviceList = new ArrayList<BLEDevice>();
        bleDeviceMap = new HashMap<String,BLEDevice>();
        deviceSet = new HashSet<String>();
        EventBus.getInstance().post(new AllBeaconsFoundEvent(deviceList));

    }

    //region StartTargetedScan
    public void startTargetedBleScan(final Context context, final boolean enable, final int minor){

        deviceList = new ArrayList<BLEDevice>();
        bleDeviceMap = new HashMap<String,BLEDevice>();
        deviceSet = new HashSet<String>();
        mHighAiLevelCallback_TargetedScan = scanCallbackHighAPILevelTargetedScan();

        targetMinor = minor;

        //initialize components
        final BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        Handler mHandler = new Handler();

        //-----------------------


        if (enable) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Scan Completed after given period
                    if (Build.VERSION.SDK_INT < 21) {
                        mBluetoothAdapter.stopLeScan(scanCallBackLowAPILevelTargetedScan);
                    } else {

                        mLEScanner.stopScan(mHighAiLevelCallback_TargetedScan);
                    }
                    //Make arraylist with BLEDevices for listView Adapter.
                    ArrayList<BLEDevice> devicesFoundDuringScann = new ArrayList<BLEDevice>();
                    for(BLEDevice device: bleDeviceMap.values()){
                        devicesFoundDuringScann.add(device);
                    }
                    //---------------------------------------------
                    if(devicesFoundDuringScann.size() == 1){
                        BLEDevice d = devicesFoundDuringScann.get(0);
                        Collections.sort(d.getRssiValues());
                        List<Integer> temp = d.getRssiValues();
                        int k = d.getRssiValues().size();
                        int min = Math.round(k * 0.3f);
                        List<Integer> temp2 = temp.subList(min,k-1);
                        d.setRssiValues(temp2);
                        //save devices measured txpower to datamanager
                        ContentManager.getInstance().setBeaconTxPower(context,Integer.toString(targetMinor),(int)d.getAverageRssi());
                    }

                    EventBus.getInstance().post(new TargetBeaconMeasuredEvent());



                }
            }, SCAN_PERIOD);


            //Start scanning--------------------------------------------------
            if (Build.VERSION.SDK_INT < 21) {

                mBluetoothAdapter.startLeScan(scanCallBackLowAPILevelTargetedScan);
            } else {
                //TODO
                Toast.makeText(context,"High level scan",Toast.LENGTH_SHORT).show();
                mLEScanner.startScan(filters, settings, mHighAiLevelCallback_TargetedScan);
            }
            //----------------------------------------------------------------

        }
        // If not enabled--
        else {
            if (Build.VERSION.SDK_INT < 21) {
                mBluetoothAdapter.stopLeScan(scanCallBackLowAPILevelTargetedScan);
            } else {
               mLEScanner.stopScan(mHighAiLevelCallback_TargetedScan);
            }
        }
        //-----------------

    }
    //endregion

//endregion

//region API LEVEL 21+ Callbacks

    //region FullScan
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private ScanCallback scanCallbackHighAPILevelFullScan(){

        return new ScanCallback() {


            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                Log.i("callbackType", String.valueOf(callbackType));
                Log.i("result", result.toString());

                BluetoothDevice d = result.getDevice();

                byte[] scanRecord;
                // Get Rssi of device
                int rssi = result.getRssi();
                String name = result.getDevice().getName();

                if (result.getScanRecord() != null) {
                    scanRecord = result.getScanRecord().getBytes();
                    int i = scanRecord.length;
                    int startByte = 2;
                    boolean patternFound = false;
                    while (startByte <= 5) {
                        if (    ((int) scanRecord[startByte + 2] & 0xff) == 0x02 && //Identifies an iBeacon
                                ((int) scanRecord[startByte + 3] & 0xff) == 0x15) { //Identifies correct data length
                            patternFound = true;
                            break;
                        }
                        startByte++;
                    }

                    if (patternFound) {
                        //Convert to hex String
                        byte[] uuidBytes = new byte[16];
                        System.arraycopy(scanRecord, startByte + 4, uuidBytes, 0, 16);
                        String hexString = BeaconMaths.getInstance().bytesToHex(uuidBytes);

                        //Here is your UUID
                        String uuid =  hexString.substring(0,8) + "-" +
                                hexString.substring(8,12) + "-" +
                                hexString.substring(12,16) + "-" +
                                hexString.substring(16,20) + "-" +
                                hexString.substring(20,32);

                        //Here is your Major value
                        int major = (scanRecord[startByte+20] & 0xff) * 0x100 + (scanRecord[startByte+21] & 0xff);

                        //Here is your Minor value
                        int minor = (scanRecord[startByte+22] & 0xff) * 0x100 + (scanRecord[startByte+23] & 0xff);

                        //Lets find calibrated Tx power
                        int txPower = (scanRecord[startByte+24]) ;

                        if(!deviceSet.contains(d.toString())){
                            deviceSet.add(d.toString());
                            //tv.append("UUID found: " + uuid + "\n");
                            //tv.append("MAJOR: " + major + "\n");
                            //tv.append("MINOR: " + minor + "\n");
                            //tv.append("txPOWER: " + txPower + "\n");
                            BLEDevice foundDevice = new BLEDevice(d.toString());
                            foundDevice.setRssiValues(new ArrayList<Integer>());
                            foundDevice.getRssiValues().add(rssi);
                            foundDevice.setMajor(major);
                            foundDevice.setMinor(minor);
                            foundDevice.setUUid(uuid);
                            foundDevice.setTxPower(txPower);
                            foundDevice.setName(name);
                            deviceList.add(foundDevice);
                            bleDeviceMap.put(d.toString(),foundDevice);
                        }else {
                            BLEDevice device = bleDeviceMap.get(d.toString());
                            device.getRssiValues().add(rssi);
                            Log.d("BLe found","BLEDevice already found");
                        }

                    }
                }
                //tv.append("---------------------" + "\n");
                //BluetoothDevice btDevice = result.getDevice();
                //connectToDevice(btDevice);
            }

            @Override
            public void onBatchScanResults(List<ScanResult> results) {
                for (ScanResult sr : results) {
                    Log.i("ScanResult - Results", sr.toString());
                }
            }

            @Override
            public void onScanFailed(int errorCode) {
                Log.e("Scan Failed", "Error Code: " + errorCode);
            }
        };

    }

    //endregionCallbacks

    //region TargetedScan
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private ScanCallback scanCallbackHighAPILevelTargetedScan(){

        return new ScanCallback() {


            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                Log.i("callbackType", String.valueOf(callbackType));
                Log.i("result", result.toString());

                BluetoothDevice d = result.getDevice();

                byte[] scanRecord;
                // Get Rssi of device
                int rssi = result.getRssi();
                String name = result.getDevice().getName();

                if (result.getScanRecord() != null) {
                    scanRecord = result.getScanRecord().getBytes();
                    int i = scanRecord.length;
                    int startByte = 2;
                    boolean patternFound = false;
                    while (startByte <= 5) {
                        if (    ((int) scanRecord[startByte + 2] & 0xff) == 0x02 && //Identifies an iBeacon
                                ((int) scanRecord[startByte + 3] & 0xff) == 0x15) { //Identifies correct data length
                            patternFound = true;
                            break;
                        }
                        startByte++;
                    }

                    if (patternFound) {
                        //Convert to hex String
                        byte[] uuidBytes = new byte[16];
                        System.arraycopy(scanRecord, startByte + 4, uuidBytes, 0, 16);
                        String hexString = BeaconMaths.getInstance().bytesToHex(uuidBytes);

                        //Here is your UUID
                        String uuid =  hexString.substring(0,8) + "-" +
                                hexString.substring(8,12) + "-" +
                                hexString.substring(12,16) + "-" +
                                hexString.substring(16,20) + "-" +
                                hexString.substring(20,32);

                        //Here is your Major value
                        int major = (scanRecord[startByte+20] & 0xff) * 0x100 + (scanRecord[startByte+21] & 0xff);

                        //Here is your Minor value
                        int minor = (scanRecord[startByte+22] & 0xff) * 0x100 + (scanRecord[startByte+23] & 0xff);

                        //Lets find calibrated Tx power
                        int txPower = (scanRecord[startByte+24]) ;

                        if(minor == targetMinor){

                            if(!deviceSet.contains(d.toString())){
                                deviceSet.add(d.toString());
                                //tv.append("UUID found: " + uuid + "\n");
                                //tv.append("MAJOR: " + major + "\n");
                                //tv.append("MINOR: " + minor + "\n");
                                //tv.append("txPOWER: " + txPower + "\n");
                                BLEDevice foundDevice = new BLEDevice(d.toString());
                                foundDevice.setRssiValues(new ArrayList<Integer>());
                                foundDevice.getRssiValues().add(rssi);
                                foundDevice.setMajor(major);
                                foundDevice.setMinor(minor);
                                foundDevice.setUUid(uuid);
                                foundDevice.setTxPower(txPower);

                                foundDevice.setName(name);
                                deviceList.add(foundDevice);
                                bleDeviceMap.put(d.toString(),foundDevice);
                            }else {
                                BLEDevice deviceAlreadyFound = bleDeviceMap.get(d.toString());
                                deviceAlreadyFound.getRssiValues().add(rssi);
                                Log.d("BLe found","BLEDevice already found");
                            }
                        }

                    }
                }
                //tv.append("---------------------" + "\n");
                //BluetoothDevice btDevice = result.getDevice();
                //connectToDevice(btDevice);
            }

            @Override
            public void onBatchScanResults(List<ScanResult> results) {
                for (ScanResult sr : results) {
                    Log.i("ScanResult - Results", sr.toString());
                }
            }

            @Override
            public void onScanFailed(int errorCode) {
                Log.e("Scan Failed", "Error Code: " + errorCode);
            }
        };

    }

    //endregion

//endregion

//region API LEVEL <21 Callbacks

    //region Fullscan
    private BluetoothAdapter.LeScanCallback scanCallBackLowAPILevelFullScan = new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi,
                             final byte[] scanRecord) {

                    BluetoothDevice d = device;

                    final int rssi_final = rssi;
                    String name = device.getName();

                    if (scanRecord != null) {
                        int i = scanRecord.length;
                        int startByte = 2;
                        boolean patternFound = false;
                        while (startByte <= 5) {
                            if (    ((int) scanRecord[startByte + 2] & 0xff) == 0x02 && //Identifies an iBeacon
                                    ((int) scanRecord[startByte + 3] & 0xff) == 0x15) { //Identifies correct data length
                                patternFound = true;
                                break;
                            }
                            startByte++;
                        }

                        if (patternFound) {
                            //Convert to hex String
                            byte[] uuidBytes = new byte[16];
                            System.arraycopy(scanRecord, startByte + 4, uuidBytes, 0, 16);
                            String hexString = BeaconMaths.getInstance().bytesToHex(uuidBytes);

                            //Here is your UUID
                            String uuid =  hexString.substring(0,8) + "-" +
                                    hexString.substring(8,12) + "-" +
                                    hexString.substring(12,16) + "-" +
                                    hexString.substring(16,20) + "-" +
                                    hexString.substring(20,32);

                            //Here is your Major value
                            int major = (scanRecord[startByte+20] & 0xff) * 0x100 + (scanRecord[startByte+21] & 0xff);

                            //Here is your Minor value
                            int minor = (scanRecord[startByte+22] & 0xff) * 0x100 + (scanRecord[startByte+23] & 0xff);

                            //Lets find calibrated Tx power
                            int txPower = (scanRecord[startByte+24]) ;

                            if(!deviceSet.contains(d.toString())){
                                deviceSet.add(d.toString());
                                //tv.append("UUID found: " + uuid + "\n");
                                //tv.append("MAJOR: " + major + "\n");
                                //tv.append("MINOR: " + minor + "\n");
                                //tv.append("txPOWER: " + txPower + "\n");
                                BLEDevice foundDevice = new BLEDevice(d.toString());
                                foundDevice.setRssiValues(new ArrayList<Integer>());
                                foundDevice.getRssiValues().add(rssi_final);
                                foundDevice.setMajor(major);
                                foundDevice.setMinor(minor);
                                foundDevice.setUUid(uuid);
                                foundDevice.setTxPower(txPower);

                                foundDevice.setName(name);
                                deviceList.add(foundDevice);
                                bleDeviceMap.put(d.toString(),foundDevice);
                            }else {
                                BLEDevice deviceAlreadyFound = bleDeviceMap.get(d.toString());
                                deviceAlreadyFound.getRssiValues().add(rssi);
                                Log.d("BLe found","BLEDevice already found");
                            }

                        }
                    }
                    //tv.append("---------------------" + "\n");
                    //BluetoothDevice btDevice = result.getDevice();
                    //connectToDevice(btDevice);


        }
    };
//endregion

    //region TargetedScan
    private BluetoothAdapter.LeScanCallback scanCallBackLowAPILevelTargetedScan = new BluetoothAdapter.LeScanCallback() {


        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi,
                             final byte[] scanRecord) {

            BluetoothDevice d = device;

            final int rssi_final = rssi;
            String name = device.getName();

            if (scanRecord != null) {
                int i = scanRecord.length;
                int startByte = 2;
                boolean patternFound = false;
                while (startByte <= 5) {
                    if (    ((int) scanRecord[startByte + 2] & 0xff) == 0x02 && //Identifies an iBeacon
                            ((int) scanRecord[startByte + 3] & 0xff) == 0x15) { //Identifies correct data length
                        patternFound = true;
                        break;
                    }
                    startByte++;
                }

                if (patternFound) {
                    //Convert to hex String
                    byte[] uuidBytes = new byte[16];
                    System.arraycopy(scanRecord, startByte + 4, uuidBytes, 0, 16);
                    String hexString = BeaconMaths.getInstance().bytesToHex(uuidBytes);

                    //Here is your UUID
                    String uuid =  hexString.substring(0,8) + "-" +
                            hexString.substring(8,12) + "-" +
                            hexString.substring(12,16) + "-" +
                            hexString.substring(16,20) + "-" +
                            hexString.substring(20,32);

                    //Here is your Major value
                    int major = (scanRecord[startByte+20] & 0xff) * 0x100 + (scanRecord[startByte+21] & 0xff);

                    //Here is your Minor value
                    int minor = (scanRecord[startByte+22] & 0xff) * 0x100 + (scanRecord[startByte+23] & 0xff);

                    //Lets find calibrated Tx power
                    int txPower = (scanRecord[startByte+24]) ;


                    if(minor == targetMinor){

                        if(!deviceSet.contains(d.toString())){
                            deviceSet.add(d.toString());
                            //tv.append("UUID found: " + uuid + "\n");
                            //tv.append("MAJOR: " + major + "\n");
                            //tv.append("MINOR: " + minor + "\n");
                            //tv.append("txPOWER: " + txPower + "\n");
                            BLEDevice foundDevice = new BLEDevice(d.toString());
                            foundDevice.setRssiValues(new ArrayList<Integer>());
                            foundDevice.getRssiValues().add(rssi_final);
                            foundDevice.setMajor(major);
                            foundDevice.setMinor(minor);
                            foundDevice.setUUid(uuid);
                            foundDevice.setTxPower(txPower);

                            foundDevice.setName(name);
                            deviceList.add(foundDevice);
                            bleDeviceMap.put(d.toString(),foundDevice);
                        }else {
                            BLEDevice deviceAlreadyFound = bleDeviceMap.get(d.toString());
                            deviceAlreadyFound.getRssiValues().add(rssi);
                            Log.d("BLe found","BLEDevice already found");
                        }
                    }

                }
            }
            //tv.append("---------------------" + "\n");
            //BluetoothDevice btDevice = result.getDevice();
            //connectToDevice(btDevice);


        }
    };
    //endregion

//endregion
}
