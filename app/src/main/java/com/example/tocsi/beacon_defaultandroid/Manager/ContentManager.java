package com.example.tocsi.beacon_defaultandroid.Manager;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.tocsi.beacon_defaultandroid.Model.Point;

import java.util.List;

/**
 * Created by Tocsi on 2015.10.29..
 */
public class ContentManager {

    private static ContentManager ourInstance = new ContentManager();

    public static ContentManager getInstance() {
        return ourInstance;
    }

    private ContentManager() {

    }

    private List<Point> mReferencePoints = null;

    public List<Point> getmReferencePoints() {
        return mReferencePoints;
    }

    public void setmReferencePoints(List<Point> mReferencePoints) {
        this.mReferencePoints = mReferencePoints;
    }

    private static final String APP_SETTINGS = "APP_SETTINGS";

    private static SharedPreferences getSharedPreferences(Context context){
        return context.getSharedPreferences(APP_SETTINGS,context.MODE_PRIVATE);
    }

    public int getBeaconTxPower(Context context, String minor){
        return getSharedPreferences(context).getInt(minor, 0);
    }

    public void setBeaconTxPower(Context context, String minor, int power){
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt(minor,power);
        editor.commit();
    }
}
