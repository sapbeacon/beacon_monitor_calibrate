package com.example.tocsi.beacon_defaultandroid.Model;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by Tocsi on 2015.11.19..
 */
public class BeaconResponse {

    private List<Point> points;

    private String mapImageUrl;

    public String getMapImageUrl() {
        return mapImageUrl;
    }

    public List<Point> getPoints() {
        return points;
    }
}
