package com.example.tocsi.beacon_defaultandroid.EventBus;

import com.squareup.otto.Bus;

/**
 * Created by Clement Tamás on 2015.09.30..
 */
public class EventBus {

    private static final EventBus instance = new EventBus();
    private Bus bus;

    private EventBus() {
        bus = new Bus();
    }

    public static EventBus getInstance() {
        return instance;
    }

    public void post(Object event) {
        bus.post(event);
    }

    public void register(Object subscriber) {
        bus.register(subscriber);
    }

    public void unregister(Object subscriber) {
        bus.unregister(subscriber
        );
    }

}