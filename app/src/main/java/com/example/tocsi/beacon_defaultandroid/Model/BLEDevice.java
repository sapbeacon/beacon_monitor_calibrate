package com.example.tocsi.beacon_defaultandroid.Model;

import com.example.tocsi.beacon_defaultandroid.Manager.BeaconMaths;

import java.util.List;

/**
 * Created by Tocsi on 2015.10.01..
 */
public class BLEDevice {

    private String name;
    private String rssi;
    private String deviceMac;
    private String UUid;
    private int Major;
    private int Minor;
    private List<Integer> rssiValues;
    private int txPower;

    public int getTxPower() {
        return txPower;
    }

    public void setTxPower(int txPower) {
        this.txPower = txPower;
    }

    public List<Integer> getRssiValues() {
        return rssiValues;
    }

    public void setRssiValues(List<Integer> rssiValues) {
        this.rssiValues = rssiValues;
    }

    public BLEDevice(String deviceMac){
        this.deviceMac = deviceMac;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRssi() {
        return rssi;
    }

    public void setRssi(String rssi) {
        this.rssi = rssi;
    }

    public String getDeviceMac() {
        return deviceMac;
    }

    public void setDeviceMac(String deviceMac) {
        this.deviceMac = deviceMac;
    }

    public String getUUid() {
        return UUid;
    }

    public void setUUid(String UUid) {
        this.UUid = UUid;
    }

    public int getMajor() {
        return Major;
    }

    public void setMajor(int major) {
        Major = major;
    }

    public int getMinor() {
        return Minor;
    }

    public void setMinor(int minor) {
        Minor = minor;
    }

    public double getAverageRssi(){
        float average = 0;
        for (Integer i : rssiValues){
            average+=i;
        }
        average = average/(rssiValues.size());
        return average;
    }

    public double getDistance(){

            return BeaconMaths.getInstance().getDistance_Average_Ratio(getAverageRssi(),txPower);

    }

    public String print(){
        StringBuilder builder = new StringBuilder();


        builder.append("Device name: ").append(this.name).append("\n")
                .append("Device MAC: ").append(this.deviceMac).append("\n")
                .append("UUID: ").append(this.UUid).append("\n")
                .append("Major: ").append(this.Major).append("\n")
                .append("Minor: ").append(this.Minor).append("\n")
                .append("txPower: ").append(this.txPower).append("\n")
                .append("measuredPower: ").append(getAverageRssi()).append("\n")
        .append("distance...: ").append(getDistance()).append("\n");
        return builder.toString();
    }

    @Override
    public String toString() {
        return Integer.toString(this.getMinor());
    }
}
