package com.example.tocsi.beacon_defaultandroid.EventBus;

import android.graphics.Bitmap;

/**
 * Created by Tocsi on 2015.11.30..
 */
public class onImageDownloadedEvent {
    private Bitmap mybm;
    public onImageDownloadedEvent(Bitmap bm){
        this.mybm = bm;
    }

    public Bitmap getMybm() {
        return mybm;
    }

    public void setMybm(Bitmap mybm) {
        this.mybm = mybm;
    }
}
