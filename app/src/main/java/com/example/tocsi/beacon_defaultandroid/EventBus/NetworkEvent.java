package com.example.tocsi.beacon_defaultandroid.EventBus;

/**
 * Created by Tocsi on 2015.11.23..
 */
public class NetworkEvent {
    private String message;

    public NetworkEvent(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
