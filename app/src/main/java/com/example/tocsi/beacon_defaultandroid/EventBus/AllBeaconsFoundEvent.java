package com.example.tocsi.beacon_defaultandroid.EventBus;

import com.example.tocsi.beacon_defaultandroid.Model.BLEDevice;

import java.util.List;
import java.util.ArrayList;

/**
 * Created by Tocsi on 2015.10.29..
 */
public class AllBeaconsFoundEvent {


    private List<BLEDevice> deviceList;

    public AllBeaconsFoundEvent (List<BLEDevice> devices){
        this.deviceList = devices;
    }

    public List<BLEDevice> getDeviceList() {
        return deviceList;
    }

    public void setDeviceList(List<BLEDevice> deviceList) {
        this.deviceList = deviceList;
    }

}
