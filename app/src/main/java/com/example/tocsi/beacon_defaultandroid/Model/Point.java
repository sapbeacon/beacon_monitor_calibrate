package com.example.tocsi.beacon_defaultandroid.Model;

import java.util.List;

/**
 * Created by Tocsi on 2015.11.23..
 */
public class Point {

    private float xcord;
    private float ycord;
    private List<BData> beaconsdata;
    private List<BdataMap> beaconsdata2;


    public List<BdataMap> getBeaconsdata2() {
        return beaconsdata2;
    }

    public void setBeaconsdata2(List<BdataMap> beaconsdata2) {
        this.beaconsdata2 = beaconsdata2;
    }

    public float getYcord() {
        return ycord;
    }

    public void setYcord(float ycord) {
        this.ycord = ycord;
    }

    public List<BData> getBeaconsdata() {
        return beaconsdata;
    }

    public void setBeaconsdata(List<BData> beaconsdata) {
        this.beaconsdata = beaconsdata;
    }

    public float getXcord() {
        return xcord;
    }

    public void setXcord(float xcord) {
        this.xcord = xcord;
    }
}
