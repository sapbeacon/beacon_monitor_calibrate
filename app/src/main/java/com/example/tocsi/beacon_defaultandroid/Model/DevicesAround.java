package com.example.tocsi.beacon_defaultandroid.Model;

import com.example.tocsi.beacon_defaultandroid.Manager.ContentManager;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Tocsi on 2015.11.26..
 */
public class DevicesAround {

    private List<BLEDevice> devicesAround = new ArrayList<>();
    private int minLengthFinal;


    private void resizeLists(){
        if (devicesAround.size() != 0){
            int minLength = devicesAround.get(0).getRssiValues().size();
            for (BLEDevice device: devicesAround){
                List<Integer> rssis = device.getRssiValues();
                int length = rssis.size();
                if (length < minLength){
                    minLength = length;
                }
            }

            minLengthFinal = minLength;
            for (BLEDevice device: devicesAround){
                List<Integer> rssis = device.getRssiValues();
                List<Integer> shortList = rssis.subList(0,minLength);
                device.setRssiValues(shortList);
            }
        }
    }

    public List<List<NameAndRSSI>> getAllMeasuredPoints(){
        resizeLists();
        List<List<NameAndRSSI>> allMeasuredPoints = new ArrayList<List<NameAndRSSI>>();
        for(int i=0; i<minLengthFinal;i++){
            List<NameAndRSSI> onePointDataList = new ArrayList<NameAndRSSI>();
            for(BLEDevice device: devicesAround){
                int value = device.getRssiValues().get(i);
                String minor = Integer.toString(device.getMinor());
                String mac = device.getDeviceMac();
                NameAndRSSI temp = new NameAndRSSI();
                temp.deviceMac = mac;
                temp.minor = minor;
                temp.rssValue = value;
                onePointDataList.add(temp);

            }
            allMeasuredPoints.add(onePointDataList);

        }
        return allMeasuredPoints;

    }


    public String findClosestPoint(List<NameAndRSSI> onePointoneRecordList){

        List<Point> referencPoints = ContentManager.getInstance().getmReferencePoints();
        Float legnagyobbValószínűség = 0f;
        Float végsőX = null;
        Float végsőY = null;
        if (referencPoints != null){
            //MINDEN EGYES REFERENCIAPONTOT VÉGIGNÉZÜNK
            for (Point actualreferencePOint: referencPoints) {
                Float adottPont_ÖsszeadottValószínűségei = 0.0f;
                Float xpoint = actualreferencePOint.getXcord();
                Float ycord = actualreferencePOint.getYcord();

                //MINDEN, egy ponthoz tartozó, beacon+ RSSI adathoz, megnézzük, hogy mennyire valószínű az a mérés ebben a referencia pontban
                for (NameAndRSSI onePointoneRecord : onePointoneRecordList) {

                    List<BdataMap> actualreferencePOintBeaconsdata2 = actualreferencePOint.getBeaconsdata2();
                    //Az adott referenciapont MINDEN mérésData-ján végigmegyünk (1 mérésdata- 1 adott beaconhöz tartozó értékeket tartalmaz)
                    for (BdataMap oneBeaconMeresData_at_ActualReferencePOint : actualreferencePOintBeaconsdata2) {
                        //Ha megvan az a mérésData ami ahhoz a Beaconhöz tartozik mint a kapott NameAndRSSI beaconje:
                        if (onePointoneRecord.minor.equals(oneBeaconMeresData_at_ActualReferencePOint.getBeaconname())) {
                            //Ennek a beaconnek keressük az adatait
                            List<JsonElement> lista = oneBeaconMeresData_at_ActualReferencePOint.getMeresdata();

                            //Végigmegyünk a beaconhoz tartozó összes RSSI értéken, keresve azt ami megyegyezik a kapott értékkel
                            for (JsonElement keyvalue : oneBeaconMeresData_at_ActualReferencePOint.getMeresdata()) {
                                String s = keyvalue.toString();
                                JsonObject o = keyvalue.getAsJsonObject();
                                String keresettérték = Integer.toString(onePointoneRecord.rssValue);
                                boolean containsKey = o.has(keresettérték);
                                //Megvan a keresett RSSI érték. Ehhez elkérjük a hozzá tartozó valószínűség értéket.
                                if (containsKey) {
                                    String valószínűség = o.get(keresettérték).toString();
                                    valószínűség.toString();
                                    adottPont_ÖsszeadottValószínűségei += Float.parseFloat(valószínűség);

                                }
                            }
                        }
                    }

                }
                if (legnagyobbValószínűség < adottPont_ÖsszeadottValószínűségei) {
                    legnagyobbValószínűség = adottPont_ÖsszeadottValószínűségei;
                    végsőX = xpoint;
                    végsőY = ycord;
                }
            }
            legnagyobbValószínűség.toString();
            if(végsőX != null && végsőY != null){
                //Megvan a legközelebbi pont!
                String resultX_Y = Float.toString(végsőX) + ";" + Float.toString(végsőY);
                return resultX_Y;

            }else {
                return null;
            }
        }else {
            return null;
        }

    }



    public List<BLEDevice> getDevices() {
        return devicesAround;
    }

    public void setDevices(List<BLEDevice> devices) {


        for(BLEDevice device: devices){
            if (device.getMinor() != 4630){
                devicesAround.add(device);
            }
        }

    }
}
