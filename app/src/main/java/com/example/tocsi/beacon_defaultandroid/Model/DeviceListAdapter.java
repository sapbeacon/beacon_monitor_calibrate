package com.example.tocsi.beacon_defaultandroid.Model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.tocsi.beacon_defaultandroid.R;

import java.util.Collection;
import java.util.List;

/**
 * Created by C5234335 on 2015.10.15..
 */
public class DeviceListAdapter extends ArrayAdapter<BLEDevice> {


    private Context context;

    private List<BLEDevice> devices;

    public DeviceListAdapter(Context context, int resource, List<BLEDevice> devices) {

        super(context, resource, devices);
        this.context = context;
        this.devices = devices;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        if(convertView == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_item,null);

            vh = new ViewHolder();
            vh.name = (TextView) convertView.findViewById(R.id.tv_name);
            vh.distance = (TextView) convertView.findViewById(R.id.tv_dist);
            vh.mac = (TextView) convertView.findViewById(R.id.tv_mac);
            vh.major = (TextView) convertView.findViewById(R.id.tv_major);
            vh.minor = (TextView) convertView.findViewById(R.id.tv_minor);
            vh.uuid = (TextView) convertView.findViewById(R.id.tv_uuid);
            vh.txpower = (TextView) convertView.findViewById(R.id.tv_txpo);
            vh.measured = (TextView) convertView.findViewById(R.id.tv_mes);
            convertView.setTag(vh);
        }else {

            vh = (ViewHolder) convertView.getTag();
        }

        BLEDevice actual = devices.get(position);

        if(actual != null){
            vh.name.setText(actual.getName());
            vh.mac.setText(actual.getDeviceMac());
            vh.distance.setText(Double.toString(actual.getDistance()));
            vh.txpower.setText(Integer.toString(actual.getTxPower()));
            vh.major.setText(Integer.toString(actual.getMajor()));
            vh.minor.setText(Integer.toString(actual.getMinor()));
            vh.uuid.setText(actual.getUUid());
            vh.measured.setText(Double.toString(actual.getAverageRssi()));

        }

        return convertView;
    }

    private class ViewHolder{

        public TextView name;
        public TextView mac;
        public TextView uuid;
        public TextView major;
        public TextView minor;
        public TextView txpower;
        public TextView measured;
        public TextView distance;

    }
}
