package com.example.tocsi.beacon_defaultandroid.Networking;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.example.tocsi.beacon_defaultandroid.EventBus.EventBus;
import com.example.tocsi.beacon_defaultandroid.EventBus.onImageDownloadedEvent;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

/**
 * Created by Tocsi on 2015.11.30..
 */
public class ImageDownloadAsynctask extends AsyncTask<String,String,Bitmap> {
    @Override
    protected Bitmap doInBackground(String... params) {
        try {
            java.net.URL url = new java.net.URL(params[0]);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        EventBus.getInstance().post(new onImageDownloadedEvent(bitmap));

        super.onPostExecute(bitmap);
    }
}
