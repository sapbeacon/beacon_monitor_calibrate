package com.example.tocsi.beacon_defaultandroid.UI;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tocsi.beacon_defaultandroid.EventBus.AllBeaconsFoundEvent;
import com.example.tocsi.beacon_defaultandroid.EventBus.EventBus;
import com.example.tocsi.beacon_defaultandroid.EventBus.NetworkEvent;
import com.example.tocsi.beacon_defaultandroid.EventBus.onImageDownloadedEvent;
import com.example.tocsi.beacon_defaultandroid.Manager.BleScanManager;
import com.example.tocsi.beacon_defaultandroid.Model.BData;
import com.example.tocsi.beacon_defaultandroid.Model.BLEDevice;
import com.example.tocsi.beacon_defaultandroid.Model.DeviceListAdapter;
import com.example.tocsi.beacon_defaultandroid.Model.DevicesAround;
import com.example.tocsi.beacon_defaultandroid.Model.NameAndRSSI;
import com.example.tocsi.beacon_defaultandroid.Model.Point;
import com.example.tocsi.beacon_defaultandroid.Networking.ImageDownloadAsynctask;
import com.example.tocsi.beacon_defaultandroid.Networking.RestClient;
import com.example.tocsi.beacon_defaultandroid.Networking.RestService;
import com.example.tocsi.beacon_defaultandroid.R;
import com.example.tocsi.beacon_defaultandroid.Service.ScannService;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class MainActivity extends AppCompatActivity {

    private BluetoothAdapter mBluetoothAdapter;
    private int REQUEST_ENABLE_BT = 1;
    private BluetoothGatt mGatt;
    private Button btnGO;
    private ListView listView;
    ProgressDialog progressDialog;
    @Bind(R.id.btn_rest)
    public Button btnRest;
    @Bind(R.id.editText)
    public EditText xcordText;
    @Bind(R.id.editText2)
    public EditText ycordText;
    @Bind(R.id.button2)
    public Button deleteRESTButton;
    @Bind(R.id.button_post)
    public Button postbutton;
    @Bind(R.id.textView7)
    public TextView tvCords;
    @Bind(R.id.textView9)
    public TextView tvMpaData;
    private static Intent serviceIntent;
    @Bind(R.id.editText3)
    public EditText mapId;
    @Bind(R.id.ivMap)
    public ImageView ivMap;



    private boolean post = false;




//region Activity methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_layout);
        ButterKnife.bind(this);
        serviceIntent = new Intent(getApplicationContext(), ScannService.class);


        btnRest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mapidString = mapId.getText().toString();
                if(mapidString.equals("")){
                    Toast.makeText(getApplicationContext(),"Set MAPID",Toast.LENGTH_SHORT).show();
                }else {
                    RestClient.getInstance().getBeaconData(mapidString);
                }

            }
        });
        deleteRESTButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RestClient.getInstance().deleteAllData();
            }
        });

        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                final String minorToCalibrate = parent.getAdapter().getItem(position).toString();
                builder1.setMessage("Do you want to calibrate this Beacon?" + "Minor: " + parent.getAdapter().getItem(position).toString());
                builder1.setCancelable(true);
                builder1.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                //TODO calibrate beacon.
                                Intent i = new Intent(getApplicationContext(),CalibrateActivity.class);
                                i.putExtra("minorValue",minorToCalibrate);
                                startActivity(i);


                            }
                        });
                builder1.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });


        btnGO = (Button) findViewById(R.id.button3);
        btnGO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //scanLeDevice(true);
                post = false;
                progressDialog = ProgressDialog.show(MainActivity.this, "Scanning...", "Collecting nearby Beacons' data...");
                BleScanManager.getInstance().setScanPeriod(10);
                //BleScanManager.getInstance().startFullBleScan(getApplicationContext(), true);
                if (!isMyServiceRunning(ScannService.class)){
                    startService(serviceIntent);
                }else {
                    stopService(serviceIntent);
                    startService(serviceIntent);
                }



            }
        });
        postbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                post = true;
                progressDialog = ProgressDialog.show(MainActivity.this, "Scanning...", "Collecting nearby Beacons' data...");
                BleScanManager.getInstance().setScanPeriod(3);//120
                BleScanManager.getInstance().startFullBleScan(getApplicationContext(),true);
            }
        });

        Button btnClear = (Button) findViewById(R.id.button4);
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BleScanManager.getInstance().clearDeviceList();

            }
        });

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "BLE Not Supported",
                    Toast.LENGTH_SHORT).show();
            finish();
        }
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
    }

    @Override
    protected void onStart() {
        EventBus.getInstance().register(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        EventBus.getInstance().unregister(this);
        super.onStop();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_CANCELED) {
                //Bluetooth not enabled.
                finish();
                return;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
//endregion

    @Subscribe
    public void onAllBeaconsFoundEvent(AllBeaconsFoundEvent event){

        List<BLEDevice> devicesFoundDuringScann = event.getDeviceList();

        if(!post){

            //TESTRUN
            DevicesAround devicesAround = new DevicesAround();
            devicesAround.setDevices(devicesFoundDuringScann);

            List<List<NameAndRSSI>> list = devicesAround.getAllMeasuredPoints();
            NameAndRSSI rssi = new NameAndRSSI();
            rssi.minor = "786";
            rssi.deviceMac = "";
            rssi.rssValue = -56;
            List<String> closesPointsList = new ArrayList<>();
            if(list != null && list.size() > 0){
                for (List<NameAndRSSI> oneTriplet: list){
                    String closestpoint = devicesAround.findClosestPoint(oneTriplet);
                    if (closestpoint != null){
                        closesPointsList.add(closestpoint);
                    }
                }

            }
            closesPointsList.toString();
            Map<String, Integer> pointsmap = new HashMap<String, Integer>();

            for (String cord: closesPointsList){
                Integer count = pointsmap.get(cord);
                pointsmap.put(cord, count != null ? count+1: 1);

            }
            String s = pointsmap.toString();
            tvMpaData.setText(s);
            //Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();

            String result = calculateCoordinate(pointsmap);
            tvCords.setText(result);



            //TESTRUN_END
        }


        JSONObject point = new JSONObject();
        Point pointObject= new Point();
        List<BData> beaconsDataList = new ArrayList<BData>();

        JSONArray beaconsdata = new JSONArray();
        String x = xcordText.getText().toString();
        String y = ycordText.getText().toString();
        if(y.equals("") || x.equals("")){

            try {
                point.put("ycord","0");
                point.put("xcord","0");
                pointObject.setXcord(0);
                pointObject.setYcord(0);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {

            try {
                point.put("xcord",x);
                point.put("ycord",y);

                pointObject.setXcord(Integer.parseInt(x));
                pointObject.setYcord(Integer.parseInt(y));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        for(BLEDevice device: devicesFoundDuringScann){
            double d = device.getAverageRssi();
            JSONObject oneBeaconData = new JSONObject();
            BData bDataObject = new BData();
            bDataObject = deviceToBdata(device);
            oneBeaconData = deviceToJSON(device);
            beaconsdata.put(oneBeaconData);
            beaconsDataList.add(bDataObject);
            //String jsonString = oneBeaconData.toString();
        }
        try {
            pointObject.setBeaconsdata(beaconsDataList);
            point.put("beaconsdata",beaconsdata);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String jsonString = point.toString();
        generateNoteOnSD("X:" + x + "_" +"Y:" + y + "_" + "Point" + ".txt",jsonString);
        //POST
        if(post){
            String mapidString = mapId.getText().toString();
            if (mapidString.equals("")){
                Toast.makeText(getApplicationContext(),"Need to set mapId",Toast.LENGTH_SHORT).show();
            }else {
                RestClient.getInstance().postPointData(pointObject,mapidString);
            }

        }
        listView.setAdapter(new DeviceListAdapter(getApplicationContext(), android.R.layout.simple_expandable_list_item_1, devicesFoundDuringScann));
        progressDialog.dismiss();

    }

    @Subscribe
    public void onNetworkEvent(NetworkEvent event){
        Snackbar.make(findViewById(android.R.id.content),event.getMessage(),Snackbar.LENGTH_LONG).show();
    }

    public JSONObject deviceToJSON(BLEDevice device){
        JSONObject object = new JSONObject();
        String name = Integer.toString(device.getMinor());
        int numberofmesurements = device.getRssiValues().size();
        List<String> stringlist = new ArrayList<String>();
        for(int i = 0; i<numberofmesurements; i++){
            stringlist.add(Integer.toString(device.getRssiValues().get(i)));
        }

        try {
            object.put("beaconname", name);
            object.put("meresdata", stringlist);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return object;
    }

    private String calculateCoordinate(Map<String, Integer> pointsmap){
        int sumOfValues = 0;
        float sumx = 0f;
        float sumy = 0f;

        for (int érték: pointsmap.values()){
            sumOfValues+=érték;
        }

        for (Map.Entry<String, Integer> entry : pointsmap.entrySet())
        {
            String kulcs = entry.getKey();
            String[] tomb = kulcs.split(";");
            String kulcsX = tomb[0];
            String kulcsY = tomb[1];
            int value = entry.getValue();
            //Kísérlet: súlyozni kell ha egyértelműen közel vagyunk egy ponthoz
            if (value > (sumOfValues/2) && value< (sumOfValues*8/10)){
                value = value + (value/4);
            }
            //kísérlet
            float actSúlyozottX = Float.parseFloat(kulcsX) * value;
            float actSúlyozottY = Float.parseFloat(kulcsY) * value;

            sumx += actSúlyozottX;
            sumy += actSúlyozottY;


                    System.out.println(entry.getKey() + "/" + entry.getValue());
        }
        float finalx = sumx/sumOfValues;
        float finaly =  sumy/sumOfValues;

        String finall_x_y = "VégsőCoord: " + Float.toString(finalx) + ";" + Float.toString(finaly);
        return  finall_x_y;
    }

    public BData deviceToBdata(BLEDevice device){
        BData bData = new BData();
        String name = Integer.toString(device.getMinor());
        int numberofmesurements = device.getRssiValues().size();
        int[] tomb;
        tomb = new int[numberofmesurements];
        for(int i = 0; i<numberofmesurements; i++){
            tomb[i] = device.getRssiValues().get(i);
        }

            bData.setBeaconname(name);
            bData.setMeresdata(tomb);



        return bData;
    }

    public void generateNoteOnSD(String sFileName, String sBody){
        try
        {
            File root = new File(Environment.getExternalStorageDirectory(), "Notes");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, sFileName);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();
            Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void onImageDownloadedEvent(onImageDownloadedEvent event){
        ivMap.setImageBitmap(event.getMybm());
    }



    @Override
    protected void onPause() {
        if (isMyServiceRunning(ScannService.class)){
            stopService(serviceIntent);
        }
        super.onPause();
    }
}
