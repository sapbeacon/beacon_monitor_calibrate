package com.example.tocsi.beacon_defaultandroid.Model;

/**
 * Created by Tocsi on 2015.11.30..
 */
public class NameAndRSSI {


        public String minor;
        public String deviceMac;
        public int rssValue;

        public String getMinor() {
            return minor;
        }

        public void setMinor(String minor) {
            this.minor = minor;
        }

        public String getDeviceMac() {
            return deviceMac;
        }

        public void setDeviceMac(String deviceMac) {
            this.deviceMac = deviceMac;
        }

        public int getRssValue() {
            return rssValue;
        }

        public void setRssValue(int rssValue) {
            this.rssValue = rssValue;
        }
    

}
